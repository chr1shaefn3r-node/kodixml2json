const { deepStrictEqual } = require('assert')
const kodixml2json = require('../../')

describe('index', () => {
  const NOOP = () => {}
  it('turns one movie videodb xml into json', (done) => {
    kodixml2json('./test/integration/testfixture_videodb_short.xml', (movie) => {
      const expected = require('./testfixture_videodb_short.json')
      deepStrictEqual([movie], expected)
      done()
    }, NOOP)
  })
})
