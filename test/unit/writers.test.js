const { strictEqual, deepStrictEqual } = require('assert')
const { writeNumber, appendArray, writeString, writeFilename, writeSetName } = require('../../lib/writers')

describe('writers', () => {
  describe('writeNumber', () => {
    it('should return a number given a number', () => {
      strictEqual(writeNumber(1), 1)
    })
    it('should return a number given a string', () => {
      strictEqual(writeNumber('1'), 1)
    })
  })
  describe('writeString', () => {
    it('should return a string given a number', () => {
      strictEqual(writeString(1), '1')
    })
    it('should return a string given a string', () => {
      strictEqual(writeString('1'), '1')
    })
  })
  describe('appendArray', () => {
    it('should return an array given undefined and a value', () => {
      deepStrictEqual(appendArray('1', undefined), ['1'])
    })
    it('should return append the value to a given array', () => {
      deepStrictEqual(appendArray('2', ['1']), ['1', '2'])
    })
  })
  describe('writeFilename', () => {
    it('should return the filename of a given url', () => {
      strictEqual(writeFilename('nfs://192.168.115.44/home/share/filme/28 Days Later/28 Days Later (2002).mkv'), '28 Days Later (2002).mkv')
      strictEqual(writeFilename('nfs://192.168.115.44/home/share/filme/Lucky # Slevin/Lucky # Slevin (2006).mkv'), 'Lucky # Slevin (2006).mkv')
    })
  })
  describe('writeSetName', () => {
    it('should remove "Filmreihe" from set name', () => {
      strictEqual(writeSetName('Star Wars Filmreihe'), 'Star Wars')
    })
    it('should remove "Collection" from set name', () => {
      strictEqual(writeSetName('The Hitman\'s Bodyguard Collection'), 'The Hitman\'s Bodyguard')
    })
    it('should remove "Filmreihe" with leading "-" from set name', () => {
      strictEqual(writeSetName('Star Trek-Filmreihe'), 'Star Trek')
      strictEqual(writeSetName('Star Trek-Filmreihe: Die Kelvin-Zeitachse'), 'Star Trek: Die Kelvin-Zeitachse')
    })
  })
})
