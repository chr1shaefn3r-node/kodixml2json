const { deepStrictEqual } = require('assert')
const extendMovieByCalculatedProperties = require('../../lib/extendMovieByCalculatedProperties')

describe('extendMovieByCalculatedProperties', () => {
  it('should set media type for known file type - dvd', () => {
    const mockMovie = getMockMovie('Movie1.m4v')
    deepStrictEqual(extendMovieByCalculatedProperties(mockMovie), { ...mockMovie, mediaType: 'dvd' })
  })
  it('should set media type for known file type - bluray', () => {
    const mockMovie = getMockMovie('Movie2.mkv')
    deepStrictEqual(extendMovieByCalculatedProperties(mockMovie), { ...mockMovie, mediaType: 'bluray' })
  })
  it('should not set media type for unknown file type', () => {
    const mockMovie = getMockMovie('Movie2.mp4')
    deepStrictEqual(extendMovieByCalculatedProperties(mockMovie), { ...mockMovie })
  })
})

function getMockMovie (file) {
  return { file }
}
