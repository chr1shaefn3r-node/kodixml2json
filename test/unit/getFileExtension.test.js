const { strictEqual } = require('assert')
const getFileExtension = require('../../lib/getFileExtension')

describe('getFileExtension', () => {
  it('should return empty string if given nothing', () => {
    strictEqual(getFileExtension(), '')
  })
  it('returns file extension for simple file', () => {
    strictEqual(getFileExtension('index.html'), '.html')
  })
  it('returns file extension for file with two dots', () => {
    strictEqual(getFileExtension('index.test.html'), '.html')
  })
})
