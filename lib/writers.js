module.exports = {
  writeNumber,
  writeString,
  appendArray,
  writeFilename,
  writeSetName
}

function writeNumber (text) {
  return Number(text)
}

function writeString (text) {
  return String(text)
}

function appendArray (value, old) {
  if (Array.isArray(old)) {
    return old.concat(value)
  }
  return [value]
}

function writeFilename (url) {
  const hashtagEscapedUrl = url.replace(/#/g, '%23')
  const { pathname } = new URL(hashtagEscapedUrl)
  const filename = pathname.substring(pathname.lastIndexOf('/') + 1)
  return decodeURIComponent(filename)
}

function writeSetName (setname) {
  return setname.replace(/[-]?Filmreihe/, '').replace('Collection', '').trim()
}
