const { writeNumber, appendArray, writeString } = require('./writers')

module.exports = {
  id: writeString,
  title: writeString,
  originaltitle: writeString,
  plot: writeString,
  tagline: writeString,
  rating: writeNumber,
  year: writeNumber,
  runtime: writeNumber,
  mpaa: writeString,
  genre: appendArray,
  country: writeString,
  director: writeString,
  studio: writeString,
  trailer: writeString,
  fanart: writeString,
  poster: writeString,
  file: writeString,
  dateadded: writeString
}
