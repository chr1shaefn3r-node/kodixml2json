const mapFileEstensionToMediaType = require('./mapFileExtensionToMediaType')
const getFileExtension = require('./getFileExtension')

module.exports = (movie = {}) => {
  const fileExtension = getFileExtension(movie.file)
  const mediaType = mapFileEstensionToMediaType[fileExtension]
  if (mediaType) {
    movie.mediaType = mediaType
  }
  return movie
}
