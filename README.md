# kodixml2json

> Cli tool to convert a kodi library export in xml to json

## CLI

```sh
$ npm install --global kodixml2json
```

```sh
$ kodixml2json --help

  Cli tool to convert a kodi library export in xml to json

  Usage:
    kodixml2json [options] FILE

  Options:
    -vv, --verbose      print usage information
    -h,  --help         print usage information
    -v,  --version      show version info and exit

  Examples:
    $ kodixml2json videodb.xml
```

## Supported Attributes

 * Movie
   * id
   * title
   * originalTitle
   * plot
   * tagline
   * rating
   * year
   * runtime
   * mpaa
   * genre
   * country
   * director
   * studio
   * trailer
   * fanart
   * poster
   * file
   * dateadded
   * mediaType
   * set
 * Actor
   * order
   * name
   * role
   * thumb

## License

MIT © [Christoph Häfner](https://christophhaefner.de)

