#!/usr/bin/env node
'use strict'
const fs = require('fs')
const meow = require('meow')
const chalk = require('chalk')
const { log } = console
const kodixml2json = require('./')

const cli = meow({
  help: [
    'Usage:',
    '  kodixml2json [options] FILE',
    '',
    'Options:',
    '  -vv, --verbose      print usage information',
    '  -h,  --help         print usage information',
    '  -v,  --version      show version info and exit',
    '',
    'Examples:',
    '  $ kodixml2json videodb.xml'
  ].join('\n')
}, {
  alias: { h: 'help', v: 'version', vv: 'verbose' },
  boolean: ['verbose']
})

if (cli.input.length === 0) {
  log(chalk.yellow('No input file given!\nUse kodixml2json -h to get usage information.'))
  process.exit(1)
}

const movies = []
const outputfileName = 'videodb.json'

kodixml2json(cli.input[0], function (movie) {
  log(`Exported movie: '${chalk.bold(movie.title)}'`)
  movies.push(movie)
}, function () {
  fs.writeFileSync(outputfileName, JSON.stringify(movies))
  log(chalk.green(`Finished! Find your result in ${outputfileName}`))
})
