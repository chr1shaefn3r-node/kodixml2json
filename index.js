#! /usr/bin/env node
'use strict'

const fs = require('fs')
const debug = require('debug')('kodixml2json')
const supportedMovieAttributes = require('./lib/supportedMovieAttributes')
const supportedMovieAttributeNames = Object.keys(supportedMovieAttributes)
const supportedActorAttributeNames = require('./lib/supportedActorAttributes')
const clone = (something) => JSON.parse(JSON.stringify(something))
const { appendArray, writeString, writeFilename, writeSetName } = require('./lib/writers')
const extendMovieByCalculatedProperties = require('./lib/extendMovieByCalculatedProperties')

const attributeRenaming = {
  filenameandpath: 'file'
}

module.exports = (inputFile, onProgress, onFinished) => {
  const strict = true // set to false for html-mode
  const options = {
    trim: true
  }
  const saxStream = require('sax').createStream(strict, options)
  let currentMovieSubtag = ''
  let movie = {}
  let actor = {}
  let set = {}
  saxStream.on('closetag', function (name) {
    if (name === 'actor') {
      movie.actors = appendArray(actor, movie.actors)
      actor = {} // reset actors to not reuse the same object for all actors
    } else if (name === 'set') {
      movie.set = set
      set = {}
    } else if (name === 'movie') {
      extendMovieByCalculatedProperties(movie)
      debug(movie)
      onProgress(clone(movie))
      movie = {} // reset movie so that e.g. array values don't add up across movies
    }
  })
  saxStream.on('opentag', function (tag) {
    if (tag.name === 'actor' || tag.name === 'set') {
      currentMovieSubtag = tag.name
    }
  })
  saxStream.on('text', function (text) {
    const currentTag = this._parser.tag.name
    // console.log(this._parser.tag)
    if (currentTag === 'filenameandpath') {
      movie[attributeRenaming['filenameandpath']] = writeFilename(text)
    }
    if (supportedMovieAttributeNames.includes(currentTag)) {
      movie[currentTag] = supportedMovieAttributes[currentTag](text, movie[currentTag])
    } else if (currentMovieSubtag === 'actor' && supportedActorAttributeNames.includes(currentTag)) {
      actor[currentTag] = writeString(text)
    } else if (currentMovieSubtag === 'set' && currentTag === 'name') {
      set.name = writeSetName(text)
    }
  })
  saxStream.on('end', onFinished)

  fs.createReadStream(inputFile)
    .pipe(saxStream)
}
